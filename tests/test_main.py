import pytest
from cicd_training.main import divide

def test_divide():
    input_numerator = 10
    input_denominator = 2
    expected_output = 5
    actual_output = divide(numerator=input_numerator, denominator=input_denominator)
    assert actual_output == expected_output
