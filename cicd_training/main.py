"""Main module containing one function as example."""

from typing import Union


def divide(numerator: Union[float, int], denominator: Union[float, int]) -> float:
    """Safly perform division with input validation.

    Args:
        numerator (Union[float, int]): The numerator for the division.
        denominator (Union[float, int]): The denominator for the division.
    
    Return:
        float: the result of the division.

    """
    try:
        numerator = float(numerator)
        denominator = float(denominator)
        if denominator == 0:
            raise ZeroDivisionError("Devision by zero is not allowed")
        result = numerator/denominator
        return result
    except (ValueError, ZeroDivisionError) as error:
        return f"Error: {error}"
